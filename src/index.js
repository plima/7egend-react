import React from 'react';
import ReactDOM from 'react-dom';

import Page1 from './Page1';
import Page2 from './Page2';
import registerServiceWorker from './registerServiceWorker';

import './ui/main.css';

ReactDOM.render(<Page1 />, document.getElementById('root'));
registerServiceWorker();
