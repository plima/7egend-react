/* Takes an API image link and normalises the file path.
  This is to avoid fetching thumbnail sized images. */
export function getImagePath(link) {
  return link.replace('_ra,w158,h184_pa,w158,h184', ''); 
};

export function capitalize(text) {
  if (text) {
    return text.replace(/(?:^|\s)\S/g, function(a) { return a.toUpperCase(); });
  }
};

export function chunk(items, size) {
  var results = [];

  while (items.length) {
    results.push(items.splice(0, size));
  }

  return results;
}

export function createBackground(image) {
  return {
    backgroundImage: `url(${image})`,
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: 'contain'
  }
}

export function sortByRating(products) {
  
  products.sort(function (a, b) {
    return parseFloat(a.rating) < parseFloat(b.rating);
  });

  return products;
}

export function createBackgroundColor(hex) {
  return {
    backgroundColor: hex
  }
}