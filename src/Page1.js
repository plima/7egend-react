import React, { Component } from 'react';
import Slider from './components/Slider';
import Modal from './components/Modal';

class Page1 extends Component {
  
  constructor(props) {
    super(props);

    this.state = {
      isModalOpen: false,
      modalTitle: '',
      modalImage: '',
      products: []
    };

    this.openModal = this.openModal.bind(this);
  }

  openModal(image, title) {
    document.body.classList.toggle('modal-open');

    this.setState({
      isModalOpen: true,
      modalImage: image,
      modalTitle: title
    });
  }

  closeModal() {
    document.body.classList.toggle('modal-open');

    this.setState({
      isModalOpen: false
    });
  }

  render() {

    return (
      <div>
        <Modal isOpen={this.state.isModalOpen} image={this.state.modalImage} title={this.state.modalTitle} closeModal={this.closeModal.bind(this)} />

        <div className='page'>
          <Slider type='blush' category='powder' title='Blush / Powder' limit={5} openModal={this.openModal} />
          <Slider type='foundation' category='powder' title='Foundation / Powder' limit={7} openModal={this.openModal} />
          <Slider type='eyeliner' category='liquid' title='Eyeliner / Liquid' limit={10} openModal={this.openModal} />
        </div>
      </div>
    );
  }
}

export default Page1;
