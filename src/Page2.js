import React, { Component } from 'react';
import ProductGrid from './components/ProductGrid';

class Page2 extends Component {

  render() {
    return (
      <ProductGrid />
    );
  }
}

export default Page2;
