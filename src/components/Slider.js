import React, { Component } from 'react';
import API from '../Api';
import Spinner from './Spinner';
import { getImagePath, capitalize } from '../helpers/Helpers';

class Slider extends Component {

  constructor(props) {
    super(props);

    this.state = {
      products: [],
      isLoading: true,
      currentSlide: 1
    };

    this.showNext = this.showNext.bind(this);
    this.showPrevious = this.showPrevious.bind(this);
    this.moveToSlide = this.moveToSlide.bind(this);
    this.getActiveProduct = this.getActiveProduct.bind(this);
    this.isActiveSlide = this.isActiveSlide.bind(this);
  }

  showNext() {
    const nextSlide = (this.state.currentSlide < this.state.products.length) ? this.state.currentSlide + 1 : 1;
    this.setState({ currentSlide: nextSlide });
  }

  showPrevious() {
    const nextSlide = (this.state.currentSlide > 1) ? this.state.currentSlide - 1 : this.state.products.length;
    this.setState({ currentSlide: nextSlide });
  }

  moveToSlide(slideIndex) {
    this.setState({ currentSlide: slideIndex });
  }

  getActiveProduct() {
    return this.state.products[this.state.currentSlide - 1];
  }

  isActiveSlide(idx) {
    return this.state.currentSlide === (idx + 1)
  }

  componentDidMount() {
    API.getProducts({
      product_type: this.props.type,
      product_category: this.props.category
    }, this.props.limit).then(data => {
      this.setState({ 
        products: data, 
        isLoading: false
      });
    });
  }

  render() {

    if (this.state.isLoading) {
      return (
        <div className='slider'>
          <div className='slider-inner'>
            <Spinner />
          </div>
        </div>
      )
    }

    return (
      <div className='slider'>
        <h3 className='slider-title'>{this.props.title}</h3>

        <div onClick={this.showNext} className='btn slider-arrow slider-arrow--next'>
          <i className='material-icons' aria-hidden='true'>keyboard_arrow_right</i>
        </div>

        <div onClick={this.showPrevious} className='btn slider-arrow slider-arrow--prev'>
          <i className='material-icons' aria-hidden='true'>keyboard_arrow_left</i>
        </div>

        <div className='slider-inner'>
          {this.state.products.map((product, idx) => {
            return <div key={product.id} className={this.isActiveSlide(idx) ? 'slider-item is-active': 'slider-item'}>
              <img onClick={() => this.props.openModal(getImagePath(product.image_link, product.name))} src={getImagePath(product.image_link)} alt={product.name} className='slider-item__image' />
            </div>;
          })}
        </div>

        <div className='slider-dots'>
          {this.state.products.map((product, idx) => {
            return <div key={idx} onClick={() => this.moveToSlide(idx + 1)} className={this.isActiveSlide(idx) ? 'slider-dot is-active': 'slider-dot'}></div>
          })}
        </div>
        
        <p className='slider-info slider-info--right'>
          <span className='slider-info__item'>{capitalize(this.getActiveProduct().name)} </span>
        </p>

        <p className='slider-info slider-info--left'>
          <span className='slider-info__item'>#{this.props.type} </span>
          <span className='slider-info__item'>#{this.props.category}</span>
        </p>
      </div>
    )
  }

}

export default Slider;