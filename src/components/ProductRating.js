import React, { Component } from 'react';

const MAX_RATING = 5;

class ProductRating extends Component {
  render() {
    const ratings = [];

    for (let i = 0; i < MAX_RATING; ++i) {
      if (i < this.props.rating) {
        ratings.push(<div className='star' key={i}>
          <i className='material-icons' aria-hidden='true'>star</i>
        </div>)
      }
    }

    return (
      <div className='product-item__rating'>
        {ratings}
      </div>
    )
  }
} 

export default ProductRating;

