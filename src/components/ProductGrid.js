import React, { Component } from 'react';
import API from '../Api';
import ProductRating from './ProductRating';
import Spinner from './Spinner';
import { 
  createBackground,
  createBackgroundColor,
  sortByRating,
  capitalize,
  chunk } from '../helpers/Helpers';

class ProductGrid extends Component {
  constructor(props) {
    super(props);

    this.state = {
      products: [],
      isLoading: true
    };
  }

  componentDidMount() {
    API.getProducts({
      product_tags: 'vegan',
      rating_greater_than: 2
    }, 30).then(data => {
      const sortedByRating = sortByRating(data, 'rating');

      this.setState({
        products: chunk(sortedByRating, 3),
        isLoading: false
      });
    });
  }

  render() {

    if (this.state.isLoading) {
      return (
        <div className='product-grid'>
          <div className='product-grid__upper'>
            <h3 className='product-grid__title'>React Product Grid</h3>
            <h6 className='product-grid__sub'>Sorted By Rating</h6>
            <Spinner />
          </div>
        </div>
      )
    }

    return (
      <div className='product-grid'>
        
        <div className='product-grid__upper'>
          <h3 className='product-grid__title'>React Product Grid</h3>
          <h6 className='product-grid__sub'>Sorted By Rating</h6>
        </div>

        {this.state.products.map((productRow, idx) => { 
          return <div className='product-row' key={idx}>

          {productRow.map((product, idx) => { 
            return <div key={product.id} className='product-grid__item'>
              <div className='product-item__image'>
                <div className='product-item__bg' style={createBackground(product.image_link)}>
                </div>
              </div>

              <div className='product-item__info'>
                <div className='product-item__name'>{capitalize(product.name)}</div>
                
                <div className='product-item__price'>{product.price}€</div>
                
                <ProductRating rating={Math.ceil(product.rating)} />

                <div className='product-item__colors'>
                  {product.product_colors.map((color, idx) => {
                    return <div className='product-item__color' key={color.hex_value} style={createBackgroundColor(color.hex_value)} title={color.colour_name}></div>
                  })}
                </div>

              </div>
            </div>
          })}
          
          </div>
        })}

      </div>
    )
  }
}

export default ProductGrid;
