import React from 'react';

const Spinner = () => (
  <div className='spinner'>
    <div className='orb'>
    </div>
  </div>
);

export default Spinner;