import React, { Component } from 'react';

class Modal extends Component {

  render() {
    return (
      <div className={this.props.isOpen ? 'modal is-active' : 'modal'}>
        <div className='modal-overlay'></div>
        <div onClick={this.props.closeModal} className='modal-close btn btn--rotate'>
          <i className='material-icons' aria-hidden='true'>close</i>
        </div>
        <img className='modal-image' alt={this.props.title} src={this.props.image} />
      </div>
    )
  }

}

export default Modal;