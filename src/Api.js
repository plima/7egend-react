const API_URL = 'http://makeup-api.herokuapp.com/api/v1/products.json';

const createQS = function(params) {
  const keyValuePairs = [];
  for (const key in params) {
    keyValuePairs.push(encodeURIComponent(key) + '=' + encodeURIComponent(params[key]));
  }
  return keyValuePairs.join('&');
};

const API = {

  getProducts(options, limit) {
    const maxResults = limit;
    const queryString = createQS(options);
    const requestUrl = `${API_URL}?${queryString}`;

    return fetch(requestUrl).then(response => response.json()).then(products => {
      return !!maxResults ? products.slice(0, maxResults) : products;
    }); 
  }

}


export default API;